import React, { Component } from 'react';
import './AddNewCard.css';

class AddNewCard extends Component {
    constructor(props){
        super(props);

        this.handleUserInput=this.handleUserInput.bind(this);
        this.handleAddNewCard=this.handleAddNewCard.bind(this);

        this.state = {
            newEnglish: '',
            newChiHan: '',
            newChiPin: '',
        }
    }

    handleUserInput(event){
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    handleAddNewCard(){
        this.props.addCard(
            this.state.newEnglish, 
            this.state.newChiHan,
            this.state.newChiPin,
        )
        console.log("handleAddNewCard hit")
        this.setState({
            newEnglish: '',
            newChiHan: '',
            newChiPin: '',
        });
    }

    render(){
        return(
            <div className="add-card-container">
                <p>Add A New Flash Card:</p>
                <div className="add-card-row">
                    <input 
                        className="new-english" 
                        name="newEnglish" 
                        placeholder="English word" 
                        value={this.state.newEnglish}
                        onChange={this.handleUserInput}
                    />
                    <input 
                        className="new-chinese-hanzi" 
                        name="newChiHan" 
                        placeholder="Hanzi character"
                        value={this.state.newChiHan}
                        onChange={this.handleUserInput}
                    />
                    <input 
                        className="new-chinese-pinyin" 
                        name="newChiPin" 
                        placeholder="Pinyin" 
                        value={this.state.newChiPin}
                        onChange={this.handleUserInput}
                    />
                    {/* Adding onClick function to this button will not allow us to respond to submit events triggered from the keyboard (ie Enter Key) */}
                    <button className='add-card-button' onClick={this.handleAddNewCard}>Add Card</button>
                </div>
            </div>
        )
    }
} 

export default AddNewCard;