import React, { Component } from 'react';
import './DrawButton.css';

class DrawButton extends Component {
    constructor(props){
        super(props);
        this.drawCard=this.drawCard.bind(this);
    }

    drawCard(){
        this.props.drawCard();
    }

    render(props){
        return(
            <div className="draw-button-container">
                <button className="draw-button" onClick={this.drawCard}>
                    Next Card
                </button>
            </div>
        )
    }
}

export default DrawButton;