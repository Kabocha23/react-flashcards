import React, { Component } from 'react';
import './App.css';
import Card from './Card/Card';
import DrawButton from './DrawButton/DrawButton';
import AddNewCard from './AddNewCard/AddNewCard'
import firebase from 'firebase/app';
import 'firebase/database';
import { DB_CONFIG } from './Config/Firebase/db_config';

class App extends Component {
  constructor(props) {
    super(props);

    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref().child('cards');
    this.updateCard=this.updateCard.bind(this);
    this.addCard=this.addCard.bind(this);

    this.state = {
      cards: [],
      currentCard: {}
    }
  }

  componentWillMount(){
    const currentCards = this.state.cards;

    this.database.on('child_added', snap => {
      currentCards.push({
        id: currentCards.length,
        english: snap.val().english,
        chinesehanzi: snap.val().chinesehanzi,
        chinesepinyin: snap.val().chinesepinyin
      })

      this.setState({
        cards: currentCards,
        currentCard: this.getRandomCard(currentCards)
      })
    })
  }

  getRandomCard(currentCards){
    let card = currentCards[Math.floor(Math.random() * currentCards.length)]
    return(card);
  }

  updateCard(){
    const currentCards = this.state.cards;
    this.setState({
      currentCard: this.getRandomCard(currentCards),
    })
  }

  addCard(eng, chihan, chipin){
    this.database.push().set({ 
      english: eng, 
      chinesehanzi: chihan, 
      chinesepinyin: chipin, 
    });

    console.log("This is the english added: " + eng)
    console.log("This is the chinesehanzi added: " + chihan)
    console.log("This is the chinesepinyin added: " + chipin)
    
  }

  render() {
    return (
      <div className="App">
        <div className='mainHeaderRow'>
          <div className='header'>Learn Chinese</div>
        </div>
        <div className="cardRow">
          <Card 
            english={this.state.currentCard.english}
            chinesehanzi={this.state.currentCard.chinesehanzi}
            chinesepinyin={this.state.currentCard.chinesepinyin}
          />
        </div>
        <div className="buttonRow">
          <DrawButton 
            drawCard={this.updateCard}
          />
        </div>
        <div className="addCardRow">
          <AddNewCard 
            addCard={this.addCard}
          />
        </div>
      </div>
    );
  }
}

export default App;
