import React from 'react';
import './Card.css';

const Card = (props) => (
    <div className="card-container">
        <div className="card">
            <div className="front">
                <div className="english">
                    {props.english}
                </div>
            </div>
            <div className="back">
                <div className="chinese-hanzi">
                    {props.chinesehanzi}
                </div>
                <div className="chinese-pinyin">
                    {props.chinesepinyin}
                </div>
            </div>
        </div>
    </div>
)

export default Card;